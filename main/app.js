function addTokens(input, tokens) {
    if (typeof (input) !== 'string' && !!input && !tokens) {
        throw new Error("Invalid input");
    }
    else if (input.length < 6 && !!input && !tokens) {
        throw new Error("Input should have at least 6 characters");
    }
    else if (!input.includes('...') && !!input && !tokens) {
        return input;
    }
    else if (!!input && !!tokens) {
        tokens.forEach(token => {
            if (typeof token === 'object' && token !== null) {
                for (let key in token) {
                    if (!(key === 'tokenName' && typeof (token.tokenName) === 'string')) {
                        throw new Error("Invalid array format")
                    }
                }
            } else {
                throw new Error("Invalid Array Format")
            }
        });
        let result = input;
        for (let i = 0; i < tokens.length; i++) {
            result = input.replace('...', '${' + tokens[i].tokenName + '}');
        }
        return result;
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;